#include <stdio.h>
#include <chrono>
#include <string>
#include <fstream>
#include <functional>
#include <iostream>
#include <vector>
#include <regex>

/* Usage

	TestWriter writer(0.1, "results.js"); // Target time per test (not including setup/teardown)

	auto testBlock = writer.test<int>("Test category");

	// Test executor - second argument matches the template argument above
	testBlock.add("Description", [](Timer* timer, int N) {
		// Setup

		int repeats = timer->start();
		for (int i = 0; i < repeats; i++) {
			// Test code here
		}
		timer->stop();

		// Teardown
	});

	testBlock.run(5); // Config value
	testBlock.run(6, "six"); // Custom label

	double referenceTime = 0.001; // Lets you compare it to algorithmic complexity
	testBlock.run(7, "seven", referenceTime); // Custom label

*/

#ifndef BENCHMARK_MAX_REPEAT_BLOCK
#define BENCHMARK_MAX_REPEAT_BLOCK 100
#endif

class Timer {
private:
	std::chrono::high_resolution_clock::time_point startTime;

	int currentRepeats;
public:
	double totalSeconds = 0;
	double targetSeconds = 1;
	int totalRepeats = 0;

	int repeats() {
		double rate = average();
		if (!rate) return 1;
		if (targetSeconds < totalSeconds) return 0;
              int repeats = (int)std::max(0.0, (targetSeconds - totalSeconds)/rate*1.1);
		return std::min(BENCHMARK_MAX_REPEAT_BLOCK, repeats);
	}
	int start() {
		startTime = std::chrono::high_resolution_clock::now();
		return currentRepeats = repeats();
	}
	void stop() {
		int repeats = currentRepeats ? currentRepeats : 1;
		std::chrono::duration<double> duration = std::chrono::high_resolution_clock::now() - startTime;
		totalSeconds += duration.count();
		totalRepeats += repeats;
	}
	double average(double defaultAverage=0) {
		return totalRepeats ? totalSeconds/totalRepeats : defaultAverage;
	}
	double seconds() {
		return totalSeconds;
	}
};

struct TestWriter;

struct Result {
       static std::string jsonQuote(std::string value) {
       	value = std::regex_replace(value, std::regex("\""), "\\\"");
       	value = std::regex_replace(value, std::regex("\n"), "\\n");
       	value = std::regex_replace(value, std::regex("\r"), "\\r");
       	return "\"" + value + "\"";
       }

       std::vector<std::string> logPairs;
       std::vector<std::string> jsonPairs;

       Result addRaw(std::string key, std::string value) const {
              Result copy = *this;
              copy.jsonPairs.push_back(jsonQuote(key) + ": " + value);
              copy.logPairs.push_back(std::regex_replace(value, std::regex("\""), ""));
              return copy;
       }
       Result add(std::string key, double value) const {
              return addRaw(key, std::to_string(value));
       }
       Result add(std::string key, int value) const {
              return addRaw(key, std::to_string(value));
       }
       Result add(std::string key, std::string value) const {
              return addRaw(key, jsonQuote(value));
       }

       void writeText(std::ostream& out) {
              for (unsigned int i = 0; i < logPairs.size(); i++) {
			if (i > 0) out << " ";
                     out << logPairs[i];
              }
              out <<  std::endl;
       }
       void writeJson(std::ostream& out) {
              out << "{";
              for (unsigned int i = 0; i < jsonPairs.size(); i++) {
			if (i > 0) out << ", ";
                     out << jsonPairs[i];
              }
              out << "}";
       }
};

// Benchmarking structure
struct TestWriter {
       struct Test {
              Result result;
              TestWriter* root;

              Test(TestWriter* root) : root(root) {};

              void run(std::function<void(Timer* timer)> testFn, double referenceFactor=1) {
                     Timer timer;
                     timer.targetSeconds = root->targetSecondsPerTest;

                     while (timer.repeats() > 0) {
                            testFn(&timer);
                     }

                     Result timedResult = result.add("rate", referenceFactor/timer.average()).add("sampleSize", timer.totalRepeats);
                     root->results.push_back(timedResult);
			unsigned int index = root->results.size();
			std::cout << index << ":\t";
                     timedResult.writeText(std::cout);
              }

              template<typename T>
              Test add(std::string key, T value) {
                     Test copy = *this;
                     copy.result = result.add(key, value);
                     return copy;
              }
		Test addRaw(std::string key, std::string value) {
                     Test copy = *this;
                     copy.result = result.addRaw(key, value);
                     return copy;
              }
       };

	double targetSecondsPerTest;
       std::string outputFile;

       std::vector<Result> results;

	std::chrono::high_resolution_clock::time_point startTime;

	TestWriter(double targetSecondsPerTest=0.1, std::string outputFile="results.js") : targetSecondsPerTest(targetSecondsPerTest), outputFile(outputFile) {
		startTime = std::chrono::high_resolution_clock::now();
	}

       Test test() {
              return Test(this);
       }

	void write() {
		std::chrono::duration<double> duration = std::chrono::high_resolution_clock::now() - startTime;
		double totalSeconds = duration.count();

		std::ofstream output;
		output.open(outputFile);

              output << "var RESULTS = // ";
		output << (int)totalSeconds << "s (" << (int)(totalSeconds/60) << "mins)";
		output << " - remove this line to get plain JSON\n[\n\t";
              for (unsigned int i = 0; i < results.size(); i++) {
                     if (i > 0) output << ",\n\t";
                     results[i].writeJson(output);
              }
              output << "\n]";

		output.close();
		std::cout << "wrote " << results.size() << " results to " << outputFile << std::endl;
	}

       ~TestWriter() {
		write();
       }
};
