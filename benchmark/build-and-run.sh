# AVX2 is enabled because KFR uses it (extremely effectively!)
# It might also use AVX512F and AVX512VL, but I couldn't get that to compile

# 1048576

g++ -std=c++14 -Wall -g -O3 -o benchmark -msse2 -mavx -mavx2 main.cpp \
	-D FFT_SIZE_LOW=16 \
	-D FFT_SIZE_HIGH=16777216 \
	-D FFT_SIZES_NON2 \
	-D SECONDS_PER_TEST=1 \
	-D TYPE_DOUBLE \
	-D TYPE_FLOAT \
	-D COMPARE_KISS_FFT \
	-D COMPARE_PFFFT others/pffft/pffft.c \
	-D COMPARE_FFTW -lfftw3 \
	-D SEARCH_FFTW_ESTIMATE \
	-D SEARCH_FFTW_MEASURE \
	-D SEARCH_FFTW_PATIENT \
	-D SEARCH_FFTW_SIZE_HIGH=131072 \
	-D COMPARE_KFR_FFT -Lothers/kfr/build/Release -lkfr_dft \
	&& echo "compiled" && ./benchmark

## For better results, run:  sudo nice -n -20 ./benchmark

# 32-bit version
#g++ -std=c++14 -Wall -g -O3 -m32 -mno-avx -mno-avx2 -o benchmark main.cpp \

# Benchmark config
#	-D SECONDS_PER_TEST=0.1 \
#	-D FFT_SIZES_NON2 \
#	-D FFT_SIZES_SMALL \
#	-D FFT_SIZES_LARGE \

# Maximum size for verification - we assume that any bugs will be present in smaller sizes too
#	-D MAX_VERIFY_SIZE=32768 \

# Signalsmith detailed config
#	-D SIGNALSMITH_FFT_PARALLEL=8 \
#	-D SIGNALSMITH_DETAILED_MAX=65536 \

# Other libraries - benchmarking or different testing methodology
#	-D COMPARE_KISS_FFT \
#	-D VERIFY_AGAINST_KISS_FFT \
#	-D COMPARE_PFFFT others/pffft/pffft.c \
#	-D COMPARE_FFTW -lfftw3 \
#	-D COMPARE_KFR_FFT -Lothers/kfr/build/Release -lkfr_dft \

# FFTW planning levels to test - MEASURE is used if none are specified
#	-D SEARCH_FFTW_ESTIMATE \
#	-D SEARCH_FFTW_MEASURE \
#	-D SEARCH_FFTW_PATIENT \
#	-D SEARCH_FFTW_EXHAUSTIVE \
# FFTW search is slow - we can either time-limit it, or stop searching for larger sizes
#	-D SEARCH_FFTW_SIZE_HIGH=131072 \
#	-D SEARCH_FFTW_TIMELIMIT=2 \
