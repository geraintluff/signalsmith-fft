#include <stdio.h>
#include <chrono>
#include <math.h>
#include <string>
#include <fstream>
#include <functional>
#include <iostream>
#include <algorithm>

#include "../signalsmith-fft.h"
#ifdef SIGNALSMITH_FFT_SIMD
#include "../signalsmith-fft-simd.h"
#endif

#ifdef COMPARE_KISS_FFT
#include "others/kissfft/kissfft.hh"
// Taken from C interface
int kiss_fft_next_fast_size(int n)
{
    while(1) {
        int m=n;
        while ( (m%2) == 0 ) m/=2;
        while ( (m%3) == 0 ) m/=3;
        while ( (m%5) == 0 ) m/=5;
        if (m<=1)
            break; /* n is completely factorable by twos, threes, and fives */
        n++;
    }
    return n;
}
#endif

#ifdef COMPARE_PFFFT
#include "others/pffft/pffft.h"
#endif

#ifdef COMPARE_FFTW
#include <fftw3.h>
#if not defined (SEARCH_FFTW_ESTIMATE) && not defined (SEARCH_FFTW_MEASURE) && not defined(SEARCH_FFTW_PATIENT) && not defined(SEARCH_FFTW_EXHAUSTIVE)
#define SEARCH_FFTW_MEASURE
#endif
#endif

#ifdef COMPARE_KFR_FFT
#include "others/kfr/include/kfr/dft/fft.hpp"
#include "others/kfr/include/kfr/version.hpp"
#endif

#include "benchmark.h"

#ifndef FFT_SIZE_LOW
#define FFT_SIZE_LOW 16
#endif
#ifndef FFT_SIZE_HIGH
#define FFT_SIZE_HIGH 32768
#endif
#if not defined (TYPE_FLOAT) and not defined (TYPE_DOUBLE)
#define TYPE_DOUBLE
#endif

#ifndef SIGNALSMITH_FFT_PARALLEL
#define SIGNALSMITH_FFT_PARALLEL 1
#endif
#ifndef SIGNALSMITH_DETAILED_MAX
#define SIGNALSMITH_DETAILED_MAX FFT_SIZE_HIGH
#endif

#ifndef SECONDS_PER_TEST
#define SECONDS_PER_TEST 0.1
#endif

#ifndef MAX_VERIFY_SIZE
#define MAX_VERIFY_SIZE 32768
#endif

template<typename sampleType>
std::vector<sampleType> randomVector(int N) {
	std::vector<sampleType> buffer;
	buffer.resize(N);
	for (int i = 0; i < N; i++) {
		buffer[i] = rand()/(sampleType)RAND_MAX - 0.5;
	}
	return buffer;
}

template<typename sampleType>
void checkCorrectness(signalsmith::fft::FFT<sampleType>* fft, int N, bool inverse, std::string label="unlabelled", double errorLimit=0) {
	if (N > MAX_VERIFY_SIZE) return;

	if (errorLimit == 0) {
		errorLimit = (sizeof(sampleType) < 8) ? 1e-6*N*log(N) : 1e-15*N*log(N);
	}

#if defined(COMPARE_KISS_FFT) && defined(VERIFY_AGAINST_KISS_FFT)
	auto buffer = randomVector<sampleType>(N*2);
	auto buffer2 = randomVector<sampleType>(N*2);
	auto buffer3 = randomVector<sampleType>(N*2);

	if (inverse) {
		fft->ipermute(buffer.data(), buffer2.data());
		fft->ifft(buffer2.data());
	} else {
		fft->fft(buffer.data(), buffer2.data());
		fft->permute(buffer2.data());
	}

	kissfft<sampleType> reference(N, inverse);
	reference.transform((std::complex<sampleType>*)buffer.data(), (std::complex<sampleType>*)buffer3.data());

	double error = 0;
	double mag2 = 0;
	for (int i = 0; i < N*2; i++) {
		double diff = buffer2[i] - buffer3[i];
		error += diff*diff;
		mag2 += buffer2[i]*buffer2[i];
	}
	error = sqrt(error/mag2);
	if (error > errorLimit) {
		std::cout << "\n" << label << ": ";
		printf("N = %i\n", N);
		printf("Results do not match KISSFFT\n");
		printf("\te = %f\n\n", error);
		for (int i = 0; i < N*2; i++) {
			printf("%i:\t%f\t%f\n", i, buffer2[i], buffer3[i]);
		}
		abort();
	}
#else
	{
		// Test single harmonic (FFT) or linear phase (IFFT)
		auto input1 = randomVector<sampleType>(N*2);
		auto output1 = randomVector<sampleType>(N*2);
		auto expected1 = randomVector<sampleType>(N*2);
		int harmonic = rand()%N;
		double phase = M_PI*2*rand()/RAND_MAX;
		for (int i = 0; i < N; i++) {
			double r = (double)i/N*harmonic;
			double p = 2*M_PI*r + phase;
			input1[2*i] = cos(p);
			input1[2*i + 1] = inverse ? -sin(p) : sin(p);

			expected1[2*i] = 0;
			expected1[2*i + 1] = 0;
			if (i == harmonic) {
				expected1[2*i] = N*cos(phase);
				expected1[2*i + 1] = N*sin(inverse ? -phase : phase);
			}
		}

		if (inverse) {
			fft->ipermute(input1.data(), output1.data());
			fft->ifft(output1.data());
		} else {
			fft->fft(input1.data(), output1.data());
			fft->permute(output1.data());
		}

		double error = 0;
		double mag2 = 0;
		for (int i = 0; i < N*2; i++) {
			double diff = output1[i] - expected1[i];
			error += diff*diff;
			mag2 += output1[i]*output1[i];
		}
		error = sqrt(error/mag2);
		if (error > errorLimit) {
			std::cout << "\n" << label << ": ";
			printf("N = %i, harmonic=%i\n", N, harmonic);
			printf("Single-harmonic result does not match\n");
			printf("\te = %.16f\n\n", error);
			for (int i = 0; i < N*2; i++) {
				printf("%i:\t%.16f\t%.16f\n", i, output1[i], expected1[i]);
			}
			abort();
		}
	}
	{
		// Test linearity of FFT/IFFT
		auto input1 = randomVector<sampleType>(N*2);
		auto input2 = randomVector<sampleType>(N*2);
		auto input12 = randomVector<sampleType>(N*2);
		for (int i = 0; i < N*2; i++) {
			input12[i] = input1[i] + input2[i];
		}

		auto output1 = randomVector<sampleType>(N*2);
		auto output2 = randomVector<sampleType>(N*2);
		auto output12 = randomVector<sampleType>(N*2);

		if (inverse) {
			fft->ipermute(input1.data(), output1.data());
			fft->ipermute(input2.data(), output2.data());
			fft->ipermute(input12.data(), output12.data());
			fft->ifft(output1.data());
			fft->ifft(output2.data());
			fft->ifft(output12.data());
		} else {
			fft->fft(input1.data(), output1.data());
			fft->fft(input2.data(), output2.data());
			fft->fft(input12.data(), output12.data());
			fft->permute(output1.data());
			fft->permute(output2.data());
			fft->permute(output12.data());
		}

		double error = 0;
		double mag2 = 0;
		for (int i = 0; i < N*2; i++) {
			double diff = output1[i] + output2[i] - output12[i];
			error += diff*diff;
			mag2 += output1[i]*output1[i];
		}
		error = sqrt(error/mag2);
		if (error > errorLimit) {
			std::cout << "\n" << label << ": ";
			printf("N = %i\n", N);
			printf("Results are not linear\n");
			printf("\te = %f\n\n", error);
			for (int i = 0; i < N*2; i++) {
				printf("%i:\t%f\t%f\t%f\n", i, output1[i], output2[i], output12[i]);
			}
			abort();
		}
	}
#endif
}

template<typename sampleType>
void checkParallel(signalsmith::fft::FFT<sampleType>* fft, bool inverse, std::string label="unlabelled", double errorLimit=0) {
	int N = fft->size();
	int parallel = fft->parallel();

	if (N > MAX_VERIFY_SIZE) return;
	if (errorLimit == 0) {
		errorLimit = (sizeof(sampleType) < 8) ? 1e-6*N*log(N) : 1e-15*N*log(N);
	}

	{
		int bufferSize = N*2*parallel;
		auto inputInterleaved = randomVector<sampleType>(bufferSize);
		auto outputInterleaved = randomVector<sampleType>(bufferSize);
		int harmonic = rand()%N;
		double phase = M_PI*2*rand()/RAND_MAX;
		for (int parallelIndex = 0; parallelIndex < parallel; parallelIndex++) {
			for (int i = 0; i < N; i++) {
				double r = (double)i/N*(harmonic + parallelIndex);
				double p = 2*M_PI*r + phase;
				inputInterleaved[2*(i*parallel + parallelIndex)] = cos(p);
				inputInterleaved[2*(i*parallel + parallelIndex) + 1] = inverse ? -sin(p) : sin(p);

				inputInterleaved[2*(i*parallel + parallelIndex)] = 2*(i*parallel + parallelIndex);
				inputInterleaved[2*(i*parallel + parallelIndex) + 1] = 2*(i*parallel + parallelIndex) + 1;
			}
		}

		if (inverse) {
			fft->ipermute(inputInterleaved.data(), outputInterleaved.data());
			fft->ifft(outputInterleaved.data(), outputInterleaved.data());
		} else {
			//fft->fft(inputInterleaved.data(), outputInterleaved.data());
			memcpy(outputInterleaved.data(), inputInterleaved.data(), bufferSize*sizeof(sampleType));
			fft->permute(outputInterleaved.data(), outputInterleaved.data());
		}

		// Copy interleaved input into series
		auto inputSeries = randomVector<sampleType>(bufferSize);
		auto outputSeries = randomVector<sampleType>(bufferSize);
		auto outputSeriesExpected = randomVector<sampleType>(bufferSize);
		for (int p = 0; p < parallel; p++) {
			for (int i = 0; i < N; i++) {
				inputSeries[(p*N + i)*2] = inputInterleaved[(p + i*parallel)*2];
				inputSeries[(p*N + i)*2 + 1] = inputInterleaved[(p + i*parallel)*2 + 1];
				outputSeriesExpected[(p*N + i)*2] = outputInterleaved[(p + i*parallel)*2];
				outputSeriesExpected[(p*N + i)*2 + 1] = outputInterleaved[(p + i*parallel)*2 + 1];
			}
		}

		fft->setSize(N, 1);
		for (int p = 0; p < parallel; p++) {
			if (inverse) {
				fft->ipermute(inputSeries.data() + 2*p*N, outputSeries.data() + 2*p*N);
				fft->ifft(outputSeries.data() + 2*p*N, outputSeries.data() + 2*p*N);
			} else {
				//fft->fft(inputSeries.data() + 2*p*N, outputSeries.data() + 2*p*N);
				memcpy(outputSeries.data() + 2*p*N, inputSeries.data() + 2*p*N, 2*N*sizeof(sampleType));
				fft->permute(outputSeries.data() + 2*p*N, outputSeries.data() + 2*p*N);
			}
		}
		fft->setSize(N, parallel);

		double error = 0;
		double mag2 = 0;
		for (int i = 0; i < bufferSize; i++) {
			double diff = outputSeries[i] - outputSeriesExpected[i];
			error += diff*diff;
			mag2 += outputSeries[i]*outputSeries[i];
		}
		error = sqrt(error/mag2);
		if (error > errorLimit) {
			std::cout << "\n" << label << ": ";
			printf("N = %i, parallel = %i, harmonic=%i\n", N, parallel, harmonic);
			printf("Single-harmonic result does not match\n");
			printf("\te = %.16f\n\n", error);
			for (int i = 0; i < bufferSize; i++) {
				printf("%i:\t%.16f\t%.16f\n", i, outputSeries[i], outputSeriesExpected[i]);
			}
			abort();
		}
	}
}

template <typename sampleType, bool inverse, bool permute, bool inPlaceFft=true, bool inPlacePermute=false>
void testSignalsmith(TestWriter::Test test, int N, int parallel=1) {
       auto fft = signalsmith::fft::getFft<sampleType>(N, parallel);
       test = test.add("impl", "Signalsmith");
       test = test.add("size", N).add("actualSize", fft->size()).add("parallel", 1);
       test = test.add("direction", inverse ? "IFFT" : "FFT");
       test = test.addRaw("permute", permute ? "true" : "false");

       test = test.addRaw("inPlace", inPlaceFft ? "true" : "false");
       test = test.addRaw("permuteInPlace", inPlacePermute ? "true" :"false");

       test.run([fft](Timer* timer){
              int N = fft->size(), parallel = fft->parallel();

              if (parallel == 1) {
                     checkCorrectness<sampleType>(fft.get(), N, inverse, "FFT (double)");
              } else {
                     checkParallel<sampleType>(fft.get(), false, "FFT (double x2)");
              }

              auto buffer = randomVector<sampleType>(2*N*parallel);
              auto buffer2 = randomVector<sampleType>(2*N*parallel);

              int repeats = timer->start();
              for (int i = 0; i < repeats; i++) {
                     if (inverse) {
                            if (permute) {
                                   if (inPlacePermute) {
                                          fft->ipermute(buffer.data());
                                   } else {
                                          fft->ipermute(buffer2.data(), buffer.data());
                                   }
                            }
                            if (inPlaceFft) {
                                   fft->ifft(buffer.data());
                            } else {
                                   fft->ifft(buffer.data(), buffer2.data());
                            }
                     } else {
                            if (inPlaceFft) {
                                   fft->fft(buffer.data());
                            } else {
                                   fft->fft(buffer.data(), buffer2.data());
                            }
                            if (permute) {
                                   if (inPlacePermute) {
                                          fft->permute(buffer2.data());
                                   } else {
                                          fft->permute(buffer.data(), buffer2.data());
                                   }
                            }
                     }
              }
              timer->stop();
       });
}

#ifdef COMPARE_KISS_FFT
template <typename sampleType, bool inverse, bool inPlace>
void testKiss(TestWriter::Test test, int N) {
       N = kiss_fft_next_fast_size(N);
       test = test.add("impl", "KISS");
       test = test.add("size", N).add("actualSize", N);
       test = test.add("direction", inverse ? "IFFT" : "FFT");
       test = test.addRaw("inPlace", inPlace ? "true" : "false");

       test.run([N](Timer* timer) {
		kissfft<sampleType> fft(N, inverse);

		auto buffer = randomVector<sampleType>(N*2);
		auto buffer2 = randomVector<sampleType>(N*2);

		int repeats = timer->start();
		for (int i = 0; i < repeats; i++) {
                     if (inPlace) {
                            fft.transform((std::complex<sampleType>*)buffer.data(), (std::complex<sampleType>*)buffer.data());
                     } else {
                            fft.transform((std::complex<sampleType>*)buffer.data(), (std::complex<sampleType>*)buffer2.data());
                     }
		}
		timer->stop();
       });
}
#endif

#ifdef COMPARE_PFFFT
float* alignPointer(float* ptr, int N) {
       size_t remainder = ((size_t)ptr)%N;
       if (remainder == 0) return ptr;
       return ptr + (N - remainder)/sizeof(float);
}
template <bool inverse, bool inPlace, bool permute>
void testPffft(TestWriter::Test test, int N) {
       using sampleType = float;

       N = signalsmith::fft::FFT<float>::findGoodSize(N/16 + (N%16 ? 1 : 0))*16;

       test = test.add("impl", "PFFFT");
       test = test.add("size", N).add("actualSize", N);
       test = test.add("direction", inverse ? "IFFT" : "FFT");
       test = test.addRaw("inPlace", inPlace ? "true" : "false");
       test = test.addRaw("permute", permute ? "true" : "false");

       test.run([N](Timer* timer) {
              pffft_direction_t direction = inverse ? PFFFT_BACKWARD : PFFFT_FORWARD;

              PFFFT_Setup * fft = pffft_new_setup(N, PFFFT_COMPLEX);

		auto buffer = randomVector<sampleType>(N*2 + 16);
		auto buffer2 = randomVector<sampleType>(N*2 + 16);
              auto work = randomVector<sampleType>(N*2 + 16);

              float* aligned = alignPointer(buffer.data(), 16);
              float* aligned2 = alignPointer(buffer2.data(), 16);
              float* alignedWork = alignPointer(work.data(), 16);

		int repeats = timer->start();
		for (int i = 0; i < repeats; i++) {
                     if (permute) {
                            pffft_transform_ordered(fft, aligned, inPlace ? aligned : aligned2, alignedWork, direction);
                     } else {
                            pffft_transform(fft, aligned, inPlace ? aligned : aligned2, alignedWork, direction);
                     }
		}
		timer->stop();

              pffft_destroy_setup(fft);
       });
}
#endif

#ifdef COMPARE_KFR_FFT
template <typename sampleType, bool inverse, bool inPlace>
void testKfr(TestWriter::Test test, int N) {
       using real = sampleType;

       N = kiss_fft_next_fast_size(N); // For fairness

       test = test.add("impl", "KFR");
       test = test.add("size", N).add("actualSize", N);
       test = test.add("direction", inverse ? "IFFT" : "FFT");
       test = test.addRaw("inPlace", inPlace ? "true" : "false");

       test.run([N](Timer* timer) {
		auto in = randomVector<sampleType>(N*2);
		auto out = randomVector<sampleType>(N*2);

              kfr::dft_plan<real> plan(N);
              unsigned char* tmp = kfr::aligned_allocate<unsigned char>(plan.temp_size);

		int repeats = timer->start();
		for (int i = 0; i < repeats; i++) {
                     if (inPlace) {
                            plan.execute(kfr::ptr_cast<kfr::complex<real>>(in.data()), kfr::ptr_cast<kfr::complex<real>>(in.data()), tmp, kfr::cbool<inverse>);
                     } else {
                            plan.execute(kfr::ptr_cast<kfr::complex<real>>(out.data()), kfr::ptr_cast<kfr::complex<real>>(in.data()), tmp, kfr::cbool<inverse>);
                     }
		}
		timer->stop();

              kfr::aligned_deallocate(tmp);
       });
}
#endif

#ifdef COMPARE_FFTW
template <bool inverse, bool inPlace>
void testFftw(TestWriter::Test test, int N, int searchLevel) {
       N = kiss_fft_next_fast_size(N); // Not an FFTW constraint, but used for fairness
       test = test.add("impl", "FFTW");
       test = test.add("size", N).add("actualSize", N);
       test = test.add("direction", inverse ? "IFFT" : "FFT");
       test = test.addRaw("inPlace", inPlace ? "true" : "false");

       if (searchLevel == FFTW_ESTIMATE) {
              test = test.add("fftwSearch", "estimate");
       } else if (searchLevel == FFTW_MEASURE) {
              test = test.add("fftwSearch", "measure");
       } else if (searchLevel == FFTW_PATIENT) {
              test = test.add("fftwSearch", "patient");
       } else if (searchLevel == FFTW_EXHAUSTIVE) {
              test = test.add("fftwSearch", "exhaustive");
       }

       int direction = inverse ? FFTW_BACKWARD : FFTW_FORWARD;

       test.run([N, direction, searchLevel](Timer* timer) {

              fftw_complex* in = (fftw_complex*) fftw_malloc(N*sizeof(fftw_complex));
              fftw_complex* out = (fftw_complex*) fftw_malloc(N*sizeof(fftw_complex));

#ifdef SEARCH_FFTW_TIMELIMIT
              fftw_set_timelimit(SEARCH_FFTW_TIMELIMIT);
#endif

              fftw_plan p = inPlace ? (
                     fftw_plan_dft_1d(N, in, in, direction, searchLevel)
              ) : (
                     fftw_plan_dft_1d(N, in, out, direction, searchLevel)
              );

              do {
                     // Reset values
                     for (int i = 0; i < N; i++) {
                            in[i][0] = rand()/(double)RAND_MAX - 0.5;
                            in[i][1] = rand()/(double)RAND_MAX - 0.5;
                     }

                     int repeats = timer->start();
                     for (int i = 0; i < repeats; i++) {
                            fftw_execute(p);
                     }
                     timer->stop();
              } while (false); // used to have timer->repeats() > 0, so we could avoid re-searching

              fftw_destroy_plan(p);

              fftw_free(in);
              fftw_free(out);
       });
}
#endif

int main() {
	TestWriter writer(SECONDS_PER_TEST, "results.js");
       auto tests = writer.test();
       auto doubleTest = tests.add("type", "double");
       auto floatTest = tests.add("type", "float");

       for (int N = FFT_SIZE_LOW; N <= FFT_SIZE_HIGH; N *= 2) {

              int maxParallel = SIGNALSMITH_FFT_PARALLEL;
              while (maxParallel > 1 && N*maxParallel > SIGNALSMITH_DETAILED_MAX) maxParallel = maxParallel>>1;

              // Non-2 sizes
              int N2 = N, N3 = N;
              while (N2 == N) {
                     N2 = N*(rand()/(double)RAND_MAX*0.75 + 0.75);
              }
              while (N3 == N || N3 == N2) {
                     N3 = N*(rand()/(double)RAND_MAX*0.75 + 0.75);
              }

#ifdef FFT_SIZES_NON2
#if defined(TYPE_DOUBLE)
              testSignalsmith<double, false, false>(doubleTest, N2, 1);
              testSignalsmith<double, false, false>(doubleTest, N3, 1);
              testSignalsmith<double, false, true>(doubleTest, N2, 1);
              testSignalsmith<double, false, true>(doubleTest, N3, 1);
#endif
#if defined(TYPE_FLOAT)
              testSignalsmith<float, false, false>(floatTest, N2, 1);
              testSignalsmith<float, false, false>(floatTest, N3, 1);
              testSignalsmith<float, false, true>(floatTest, N2, 1);
              testSignalsmith<float, false, true>(floatTest, N3, 1);
#endif
#endif

              for (int parallel = 1; parallel <= maxParallel; parallel *= 2) {
#if defined(TYPE_DOUBLE)
                     // With and without de-permutation, forward and backward
                     testSignalsmith<double, false, false>(doubleTest, N, parallel);
                     testSignalsmith<double, true, false>(doubleTest, N, parallel);
                     testSignalsmith<double, false, true>(doubleTest, N, parallel);
                     testSignalsmith<double, true, true>(doubleTest, N, parallel);
                     if (parallel == 1) {
                            testSignalsmith<double, false, false, false>(doubleTest, N, parallel);
                            testSignalsmith<double, false, true, false, true>(doubleTest, N, parallel);
                            testSignalsmith<double, false, true, true, true>(doubleTest, N, parallel);
                            testSignalsmith<double, true, true, true, true>(doubleTest, N, parallel);
                     }
#endif
#if defined(TYPE_FLOAT)
                     testSignalsmith<float, false, false>(floatTest, N, parallel);
                     testSignalsmith<float, true, false>(floatTest, N, parallel);
                     testSignalsmith<float, false, true>(floatTest, N, parallel);
                     testSignalsmith<float, true, true>(floatTest, N, parallel);
                     if (parallel == 1) {
                            testSignalsmith<float, false, false, false>(floatTest, N, parallel);
                            testSignalsmith<float, false, true, false, true>(floatTest, N, parallel);
                            testSignalsmith<float, false, true, true, true>(floatTest, N, parallel);
                            testSignalsmith<float, true, true, true, true>(floatTest, N, parallel);
                     }
#endif
              } // parallel loop

#if defined(COMPARE_FFTW) && defined(TYPE_DOUBLE)
              { // We only test double-precision FFTW
                     int maxSearchSize = FFT_SIZE_HIGH;
#ifdef SEARCH_FFTW_SIZE_HIGH
                     maxSearchSize = SEARCH_FFTW_SIZE_HIGH;
#endif
#ifdef SEARCH_FFTW_ESTIMATE
                     testFftw<false, false>(doubleTest, N, FFTW_ESTIMATE);
                     testFftw<true, false>(doubleTest, N, FFTW_ESTIMATE);
                     testFftw<false, true>(doubleTest, N, FFTW_ESTIMATE);
#ifdef FFT_SIZES_NON2
                     testFftw<false, false>(doubleTest, N2, FFTW_ESTIMATE);
                     testFftw<false, false>(doubleTest, N3, FFTW_ESTIMATE);
#endif
#endif
                     if (N <= maxSearchSize) {
#ifdef SEARCH_FFTW_MEASURE
                            testFftw<false, false>(doubleTest, N, FFTW_MEASURE);
                            testFftw<true, false>(doubleTest, N, FFTW_MEASURE);
                            testFftw<false, true>(doubleTest, N, FFTW_MEASURE);
#ifdef FFT_SIZES_NON2
                            testFftw<false, false>(doubleTest, N2, FFTW_MEASURE);
                            testFftw<false, false>(doubleTest, N3, FFTW_MEASURE);
#endif
#endif
#ifdef SEARCH_FFTW_PATIENT
                            testFftw<false, false>(doubleTest, N, FFTW_PATIENT);
                            testFftw<true, false>(doubleTest, N, FFTW_PATIENT);
                            testFftw<false, true>(doubleTest, N, FFTW_PATIENT);
#ifdef FFT_SIZES_NON2
                            testFftw<false, false>(doubleTest, N2, FFTW_PATIENT);
                            testFftw<false, false>(doubleTest, N3, FFTW_PATIENT);
#endif
#endif
#ifdef SEARCH_FFTW_EXHAUSTIVE
                            testFftw<false, false>(doubleTest, N, FFTW_EXHAUSTIVE);
                            testFftw<true, false>(doubleTest, N, FFTW_EXHAUSTIVE);
                            testFftw<false, true>(doubleTest, N, FFTW_EXHAUSTIVE);
#ifdef FFT_SIZES_NON2
                            testFftw<false, false>(doubleTest, N2, FFTW_EXHAUSTIVE);
                            testFftw<false, false>(doubleTest, N3, FFTW_EXHAUSTIVE);
#endif
#endif
                     }
              }
#endif

#ifdef COMPARE_KISS_FFT
              {
#if defined(TYPE_DOUBLE)
                     testKiss<double, false, false>(doubleTest, N);
                     testKiss<double, true, false>(doubleTest, N);
                     testKiss<double, false, true>(doubleTest, N);
#ifdef FFT_SIZES_NON2
                     testKiss<double, false, false>(doubleTest, N2);
                     testKiss<double, false, false>(doubleTest, N3);
#endif
#endif
#if defined(TYPE_FLOAT)
                     testKiss<float, false, false>(floatTest, N);
                     testKiss<float, true, false>(floatTest, N);
                     testKiss<float, false, true>(floatTest, N);
#ifdef FFT_SIZES_NON2
                     testKiss<float, false, false>(floatTest, N2);
                     testKiss<float, false, false>(floatTest, N3);
#endif
#endif
              }
#endif

#if defined(COMPARE_PFFFT) && defined(TYPE_FLOAT)
              {
                     testPffft<false, false, false>(floatTest, N);
                     testPffft<false, false, true>(floatTest, N);
                     testPffft<false, true, false>(floatTest, N);
                     testPffft<false, true, true>(floatTest, N);
/*
#ifdef FFT_SIZES_NON2
                     testPffft<false, false, false>(floatTest, N2);
                     testPffft<false, false, false>(floatTest, N3);
                     testPffft<false, false, true>(floatTest, N2);
                     testPffft<false, false, true>(floatTest, N3);
#endif
*/
              }
#endif

#ifdef COMPARE_KFR_FFT
              {
#if defined(TYPE_DOUBLE)
                     testKfr<double, false, false>(doubleTest, N);
                     testKfr<double, false, true>(doubleTest, N);
                     testKfr<double, true, false>(doubleTest, N);
#ifdef FFT_SIZES_NON2
                     testKfr<double, false, false>(doubleTest, N2);
                     testKfr<double, false, false>(doubleTest, N3);
#endif
#endif
              }
#endif
              writer.write(); // To keep intermediate results, in case we have to quit early
       } // N loop

	return 0;
}
